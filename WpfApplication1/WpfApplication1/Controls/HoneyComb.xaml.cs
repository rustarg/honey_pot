﻿namespace WpfApplication1.Controls
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for HoneyComb.xaml
    /// </summary>
    public partial class HoneyComb : UserControl
    {
        public int num = 0;

        public HoneyComb()
        {
            this.InitializeComponent();
        }

        public HoneyComb(double w, double h)
        {
            this.InitializeComponent();
            this.honeycomb.Width = w;
            this.honeycomb.Height = h;
        }

        public bool IsSet { get; set; } = false;

        public HoneyComb NextHor { get; set; }

        public HoneyComb NextLeft { get; set; }

        public HoneyComb NextRight { get; set; }

        public HoneyComb PrevHor { get; set; }

        public HoneyComb PrevLeft { get; set; }

        public HoneyComb PrevRight { get; set; }

        public int[] Score { get; set; }
    }
}