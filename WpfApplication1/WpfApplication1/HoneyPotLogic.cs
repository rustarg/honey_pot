﻿namespace WpfApplication1
{
    internal class HoneyPotLogic
    {
        private readonly int[] bottomEdge = { 45, 46, 47, 48, 49, 50, 51, 52, 53, 54 };

        public readonly int[] horRows =
            {
                1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7,
                7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10,
                10, 10, 10, 10, 10
            };

        private readonly int[] leftEdge = { 0, 1, 3, 6, 10, 15, 21, 28, 36, 45 };

        public readonly int[] leftRows =
            {
                10, 9, 10, 8, 9, 10, 7, 8, 9, 10, 6, 7, 8, 9, 10, 5, 6, 7, 8, 9, 10, 4, 5, 6,
                7, 8, 9, 10, 3, 4, 5, 6, 7, 8, 9, 10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3,
                4, 5, 6, 7, 8, 9, 10
            };

        private readonly int[] rightEdge = { 0, 2, 5, 9, 14, 20, 27, 35, 44, 54 };

        public readonly int[] rightRows =
            {
                10, 10, 9, 10, 9, 8, 10, 9, 8, 7, 10, 9, 8, 7, 6, 10, 9, 8, 7, 6, 5, 10, 9,
                8, 7, 6, 5, 4, 10, 9, 8, 7, 6, 5, 4, 3, 10, 9, 8, 7, 6, 5, 4, 3, 2, 10, 9,
                8, 7, 6, 5, 4, 3, 2, 1
            };

        public readonly bool[,] scores = new bool[3, 10];

        public int[] Evaluate(int move, ulong position)
        {
            // if position occupied - return empty array
            if ((position & ((ulong)1 << move)) != 0)
            {
                return new int[0];
            }

            // find movement row
            var row = 0;
            foreach (var e in this.leftEdge)
            {
                if (move >= e)
                {
                    row++;
                }
                else
                {
                    break;
                }
            }

            return new[]
                       {
                           this.EvaluateHor(move, position, row), this.EvaluateLeft(move, position, row),
                           this.EvaluateRight(move, position, row)
                       };
        }

        public int[] FindEdges(int move)
        {
            var result = new int[6];

            // find movement row
            var row = 0;
            foreach (var e in this.leftEdge)
            {
                if (move >= e)
                {
                    row++;
                }
                else
                {
                    break;
                }
            }

            result[0] = this.leftEdge[row - 1];
            result[1] = this.rightEdge[row - 1];

            result[2] = this.rightEdge[move - this.leftEdge[row - 1]];
            result[3] = this.bottomEdge[move - this.leftEdge[row - 1]];

            result[4] = this.leftEdge[row - move + this.leftEdge[row - 1] - 1];
            result[5] = this.bottomEdge[10 - row + move - this.leftEdge[row - 1]];

            return result;
        }

        private int EvaluateHor(int move, ulong position, int row)
        {
            var score = 0;
            var line = true;
            var curMove = move;

            while (curMove != this.leftEdge[row - 1])
            {
                curMove--;
                if ((position & ((ulong)1 << curMove)) == 0)
                {
                    line = false;
                    score = 0;
                    break;
                }

                score++;
            }

            if (line)
            {
                curMove = move;

                while (curMove != this.rightEdge[row - 1])
                {
                    curMove++;
                    if ((position & ((ulong)1 << curMove)) == 0)
                    {
                        line = false;
                        score = 0;
                        break;
                    }

                    score++;
                }
            }

            if (line && score > 0 && !this.scores[0, row])
            {
                return ++score;
            }

            return 0;
        }

        private int EvaluateLeft(int move, ulong position, int row)
        {
            var score = 0;
            var line = true;
            var curMove = move;
            var curRow = row;

            while (curMove != this.rightEdge[move - this.leftEdge[row - 1]])
            {
                curRow--;
                curMove -= curRow;
                if ((position & ((ulong)1 << curMove)) == 0)
                {
                    line = false;
                    score = 0;
                    break;
                }

                score++;
            }

            if (line)
            {
                curMove = move;
                curRow = row;

                while (curMove != this.bottomEdge[move - this.leftEdge[row - 1]])
                {
                    curMove += curRow;
                    curRow++;
                    if ((position & ((ulong)1 << curMove)) == 0)
                    {
                        line = false;
                        score = 0;
                        break;
                    }

                    score++;
                }
            }

            if (line && score > 0)
            {
                return ++score;
            }

            return 0;
        }

        private int EvaluateRight(int move, ulong position, int row)
        {
            var score = 0;
            var line = true;
            var curMove = move;
            var curRow = row;

            while (curMove != this.leftEdge[row - move + this.leftEdge[row - 1] - 1])
            {
                curMove -= curRow;
                curRow--;
                if ((position & ((ulong)1 << curMove)) == 0)
                {
                    line = false;
                    score = 0;
                    break;
                }

                score++;
            }

            if (line)
            {
                curMove = move;
                curRow = row;

                while (curMove != this.bottomEdge[10 - row + move - this.leftEdge[row - 1]])
                {
                    curRow++;
                    curMove += curRow;
                    if ((position & ((ulong)1 << curMove)) == 0)
                    {
                        line = false;
                        score = 0;
                        break;
                    }

                    score++;
                }
            }

            if (line && score > 0)
            {
                return ++score;
            }

            return 0;
        }
    }
}