﻿namespace WpfApplication1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;

    using WpfApplication1.Controls;

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private const PenLineCap ResLineStrokeLineCap = PenLineCap.Round;

        private const int ResLineStrokeThickness = 10;

        private readonly HoneyPotLogic gameLogic1 = new HoneyPotLogic();

        // cross line properties
        private readonly Brush resLineStroke = new SolidColorBrush(Color.FromRgb(0, 20, 100));

        private int compScore;

        private Storyboard fadeIn;

        private Storyboard fadeOut;

        private Storyboard hintSb;

        private HoneyComb[] honeyPot;

        private Line horLine;

        private bool isMyTurn = true;

        private Line leftLine;

        private Line rightLine = new Line();

        private ulong state;

        private int youScore;

        public MainWindow()
        {
            this.InitializeComponent();
            this.Loaded += this.MainWindow_Loaded;

            this.InitGame();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public int CompScore
        {
            get
            {
                return this.compScore;
            }

            set
            {
                this.compScore = value;
                this.OnPropertyChanged("CompScore");
            }
        }

        public int Hint { get; set; }

        public double HintLeft { get; set; }

        public double HintTop { get; set; }

        public int Score { get; set; }

        public string State => Convert.ToString((long)this.state, 2);

        public int YouScore
        {
            get
            {
                return this.youScore;
            }

            set
            {
                this.youScore = value;
                this.OnPropertyChanged("YouScore");
            }
        }

        protected void OnPropertyChanged(string name)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private static Path CreatePath(int rotation, Brush color, string name)
        {
            var segments = new PathSegmentCollection
                               {
                                   new LineSegment(
                                       new Point(71.1264635579282, 87.9879930253477),
                                       true),
                                   new LineSegment(
                                       new Point(120.282160660181, 58.9206130901574),
                                       true),
                                   new LineSegment(
                                       new Point(95.755894417646, 44.4347144010695),
                                       true),
                                   new LineSegment(
                                       new Point(95.7684302027121, 102.533561706543),
                                       true)
                               };
            var figures = new List<PathFigure>
                              {
                                  new PathFigure
                                      {
                                          Segments = segments,
                                          IsClosed = true,
                                          IsFilled = true,
                                          StartPoint =
                                              new Point(95.7684302027121, 102.533561706543)
                                      }
                              };
            var hover1 = new Path { Data = new PathGeometry(figures) };
            Canvas.SetLeft(hover1, -70);
            Canvas.SetTop(hover1, -44);
            Panel.SetZIndex(hover1, 10000);
            hover1.Stroke = Brushes.Green;
            hover1.StrokeLineJoin = PenLineJoin.Round;
            hover1.StrokeThickness = 2;
            hover1.Fill = color;
            hover1.Opacity = 0.8;
            hover1.RenderTransformOrigin = new Point(0.79, 0.705);
            hover1.RenderTransform = new RotateTransform(rotation);
            hover1.Name = name;
            hover1.Opacity = 0;
            hover1.IsHitTestVisible = false;
            return hover1;
        }

        private static Path FindPathByName(string name, UIElementCollection children)
        {
            Path result = null;
            foreach (UIElement ue in children)
            {
                result = ue as Path;
                if (result == null)
                {
                    continue;
                }

                if (result.Name == name)
                {
                    break;
                }
                else
                {
                    result = null;
                }
            }

            return result;
        }

        private void DrawHoneyPot()
        {
            this.honeyPot = new HoneyComb[55];

            for (var i = 0; i < this.honeyPot.Length; i++)
            {
                this.honeyPot[i] = new HoneyComb(50, 57.735) { num = i };
            }

            var n = 0;
            for (var i = 1; i < 11; i++)
            {
                for (var j = 0; j < i; j++, n++)
                {
                    // set NextHor
                    if (j < i - 1)
                    {
                        this.honeyPot[n].NextHor = this.honeyPot[n + 1];
                    }

                    // set PrevHor
                    if (j > 0)
                    {
                        this.honeyPot[n].PrevHor = this.honeyPot[n - 1];
                    }

                    // set NextLeft, NextRight
                    if (i < 10)
                    {
                        this.honeyPot[n].NextLeft = this.honeyPot[n + i];
                        this.honeyPot[n].NextRight = this.honeyPot[n + i + 1];
                    }

                    // set PrevtLeft, PrevRight
                    if (i > 1)
                    {
                        // set PrevRight
                        if (j > 0)
                        {
                            this.honeyPot[n].PrevLeft = this.honeyPot[n - i];
                        }

                        if (j < i - 1)
                        {
                            this.honeyPot[n].PrevRight = this.honeyPot[n - i + 1];
                        }
                    }

                    Canvas.SetLeft(
                        this.honeyPot[n],
                        (this.canvas.ActualWidth / 2) - (i * this.honeyPot[n].honeycomb.Width / 2) + 2
                        + (j * this.honeyPot[n].honeycomb.Width));
                    Canvas.SetTop(this.honeyPot[n], i * 44);
                    this.honeyPot[n].canvas1.MouseLeftButtonDown += this.H1MouseLeftButtonDown;
                    this.honeyPot[n].canvas1.MouseEnter += this.H1MouseEnter;
                    this.honeyPot[n].canvas1.MouseLeave += this.H1MouseLeave;

                    // Path hover1 = CreatePath(60, Brushes.Bisque);
                    var ph = CreatePath(60, Brushes.Bisque, "horHintArea");
                    ph.MouseLeftButtonDown += this.HorLeftButtonUp;
                    ph.MouseEnter += this.Ph_MouseEnter;
                    ph.MouseLeave += this.Ph_MouseLeave;
                    var pl = CreatePath(0, Brushes.PaleVioletRed, "leftHintArea");
                    pl.MouseLeftButtonDown += this.LeftLeftButtonUp;
                    pl.MouseEnter += this.Pl_MouseEnter;
                    pl.MouseLeave += this.Ph_MouseLeave;
                    var pr = CreatePath(120, Brushes.RoyalBlue, "rightHintArea");
                    pr.MouseLeftButtonDown += this.rightLeftButtonUp;
                    pr.MouseEnter += this.Pr_MouseEnter;
                    pr.MouseLeave += this.Ph_MouseLeave;
                    this.honeyPot[n].canvas1.Children.Add(ph);
                    this.honeyPot[n].canvas1.Children.Add(pl);
                    this.honeyPot[n].canvas1.Children.Add(pr);

                    this.canvas.Children.Add(this.honeyPot[n]);
                }
            }
        }

        private void DrawHorLine(int[] score, int num)
        {
            var edges = this.gameLogic1.FindEdges(num);
            if (score[0] == 0)
            {
                return;
            }

            this.horLine = new Line
                               {
                                   X1 =
                                       Canvas.GetLeft(this.honeyPot[edges[0]])
                                       + (this.honeyPot[edges[0]].honeycomb.ActualWidth / 2),
                                   Y1 =
                                       Canvas.GetTop(this.honeyPot[edges[0]])
                                       + (this.honeyPot[edges[0]].honeycomb.ActualHeight / 2)
                               };
            this.horLine.X2 = this.horLine.X1;
            this.horLine.Y2 = this.horLine.Y1;

            var x2 = Canvas.GetLeft(this.honeyPot[edges[1]]) + (this.honeyPot[edges[1]].honeycomb.ActualWidth / 2);

            this.horLine.Stroke = this.resLineStroke;
            this.horLine.StrokeThickness = ResLineStrokeThickness;
            this.horLine.StrokeEndLineCap = ResLineStrokeLineCap;
            this.horLine.StrokeStartLineCap = ResLineStrokeLineCap;

            var sbh = new Storyboard();

            if (Math.Abs(x2) > 0.01)
            {
                var da1 = new DoubleAnimation(this.horLine.X2, x2, new Duration(new TimeSpan(0, 0, 0, 0, 540)));
                Storyboard.SetTargetProperty(da1, new PropertyPath("(Line.X2)"));
                da1.EasingFunction = new ExponentialEase { Exponent = 9 };
                sbh.Children.Add(da1);
            }

            this.canvas.Children.Add(this.horLine);
            this.horLine.BeginStoryboard(sbh);
        }

        private void DrawLeftLine(IReadOnlyList<int> score, int num)
        {
            var edges = this.gameLogic1.FindEdges(num);
            if (score[1] == 0)
            {
                return;
            }

            this.leftLine = new Line
                                {
                                    X1 =
                                        Canvas.GetLeft(this.honeyPot[edges[2]])
                                        + (this.honeyPot[edges[2]].honeycomb.ActualWidth / 2),
                                    Y1 =
                                        Canvas.GetTop(this.honeyPot[edges[2]])
                                        + (this.honeyPot[edges[2]].honeycomb.ActualHeight / 2)
                                };

            this.leftLine.X2 = this.leftLine.X1;
            this.leftLine.Y2 = this.leftLine.Y1;

            var x2 = Canvas.GetLeft(this.honeyPot[edges[3]]) + (this.honeyPot[edges[3]].honeycomb.ActualWidth / 2);
            var y2 = Canvas.GetTop(this.honeyPot[edges[3]]) + (this.honeyPot[edges[3]].honeycomb.ActualHeight / 2);

            this.leftLine.Stroke = this.resLineStroke;
            this.leftLine.StrokeThickness = ResLineStrokeThickness;
            this.leftLine.StrokeEndLineCap = ResLineStrokeLineCap;
            this.leftLine.StrokeStartLineCap = ResLineStrokeLineCap;

            var sbl = new Storyboard();

            if (Math.Abs(x2) > 0.01)
            {
                var da1 = new DoubleAnimation(this.leftLine.X2, x2, new Duration(new TimeSpan(0, 0, 0, 0, 540)));
                Storyboard.SetTargetProperty(da1, new PropertyPath("(Line.X2)"));
                da1.EasingFunction = new ExponentialEase { Exponent = 9 };
                sbl.Children.Add(da1);
            }

            if (Math.Abs(y2) > 0.01)
            {
                var da = new DoubleAnimation(this.leftLine.Y2, y2, new Duration(new TimeSpan(0, 0, 0, 0, 540)));
                Storyboard.SetTargetProperty(da, new PropertyPath("(Line.Y2)"));
                da.EasingFunction = new ExponentialEase { Exponent = 9 };
                sbl.Children.Add(da);
            }

            this.canvas.Children.Add(this.leftLine);
            this.leftLine.BeginStoryboard(sbl);
        }

        private void DrawLines(int num, int[] score)
        {
            this.DrawHorLine(score, num);
            this.DrawLeftLine(score, num);
            this.DrawRightLine(score, num);
        }

        private void DrawRightLine(IReadOnlyList<int> score, int num)
        {
            var edges = this.gameLogic1.FindEdges(num);
            if (score[2] == 0)
            {
                return;
            }

            this.rightLine = new Line
                                 {
                                     X1 =
                                         Canvas.GetLeft(this.honeyPot[edges[4]])
                                         + (this.honeyPot[edges[4]].honeycomb.ActualWidth / 2),
                                     Y1 =
                                         Canvas.GetTop(this.honeyPot[edges[4]])
                                         + (this.honeyPot[edges[4]].honeycomb.ActualHeight / 2)
                                 };

            this.rightLine.X2 = this.rightLine.X1;
            this.rightLine.Y2 = this.rightLine.Y1;

            var x2 = Canvas.GetLeft(this.honeyPot[edges[5]]) + (this.honeyPot[edges[5]].honeycomb.ActualWidth / 2);
            var y2 = Canvas.GetTop(this.honeyPot[edges[5]]) + (this.honeyPot[edges[5]].honeycomb.ActualHeight / 2);

            this.rightLine.Stroke = this.resLineStroke;
            this.rightLine.StrokeThickness = ResLineStrokeThickness;
            this.rightLine.StrokeEndLineCap = ResLineStrokeLineCap;
            this.rightLine.StrokeStartLineCap = ResLineStrokeLineCap;

            var sbl = new Storyboard();

            if (Math.Abs(x2) > 0.01)
            {
                var da1 = new DoubleAnimation(this.rightLine.X2, x2, new Duration(new TimeSpan(0, 0, 0, 0, 540)));
                Storyboard.SetTargetProperty(da1, new PropertyPath("(Line.X2)"));
                da1.EasingFunction = new ExponentialEase { Exponent = 9 };
                sbl.Children.Add(da1);
            }

            if (Math.Abs(y2) > 0.01)
            {
                var da = new DoubleAnimation(this.rightLine.Y2, y2, new Duration(new TimeSpan(0, 0, 0, 0, 540)));
                Storyboard.SetTargetProperty(da, new PropertyPath("(Line.Y2)"));
                da.EasingFunction = new ExponentialEase { Exponent = 9 };
                sbl.Children.Add(da);
            }

            this.canvas.Children.Add(this.rightLine);
            this.rightLine.BeginStoryboard(sbl);
        }

        private void H1MouseEnter(object sender, MouseEventArgs e)
        {
            var honeyComb = ((Canvas)sender).Parent as HoneyComb;
            if (honeyComb != null && honeyComb.IsSet)
            {
                return;
            }

            var sb = honeyComb?.FindResource("OnMouseEnter") as Storyboard;
            sb?.Begin();

            this.hint.Visibility = Visibility.Visible;

            if (honeyComb == null)
            {
                return;
            }

            honeyComb.Score = this.gameLogic1.Evaluate(honeyComb.num, this.state);

            if (honeyComb.Score.Count() <= 0)
            {
                return;
            }

            // horizontal hint present
            if (honeyComb.Score[0] > 0 && honeyComb.Score[1] + honeyComb.Score[2] > 0)
            {
                var horHintPath = FindPathByName("horHintArea", honeyComb.canvas1.Children);
                horHintPath?.BeginStoryboard(this.fadeIn);

                if (horHintPath != null)
                {
                    horHintPath.IsHitTestVisible = true;
                }
            }

            // left hint present
            if (honeyComb.Score[1] > 0 && honeyComb.Score[0] + honeyComb.Score[2] > 0)
            {
                var leftHintPath = FindPathByName("leftHintArea", honeyComb.canvas1.Children);
                if (leftHintPath != null) leftHintPath.BeginStoryboard(this.fadeIn);

                leftHintPath.IsHitTestVisible = true;
            }

            // right hint present
            if (honeyComb.Score[2] > 0 && honeyComb.Score[0] + honeyComb.Score[1] > 0)
            {
                var rightHintPath = FindPathByName("rightHintArea", honeyComb.canvas1.Children);
                if (rightHintPath != null) rightHintPath.BeginStoryboard(this.fadeIn);

                rightHintPath.IsHitTestVisible = true;
            }

            this.Hint = honeyComb.Score.Sum();
            this.OnPropertyChanged("Hint");

            this.HintLeft = Canvas.GetLeft(honeyComb) + (honeyComb.honeycomb.ActualWidth / 2) - 7
                            - (this.Hint > 9 ? 5 : 0);
            this.HintTop = Canvas.GetTop(honeyComb) + (honeyComb.honeycomb.ActualHeight / 2) - 10;
            this.OnPropertyChanged("HintTop");
            this.OnPropertyChanged("HintLeft");
            this.hint.BeginStoryboard(this.hintSb);
        }

        private void H1MouseLeave(object sender, MouseEventArgs e)
        {
            var honeyComb = ((Canvas)sender).Parent as HoneyComb;
            if (honeyComb != null && honeyComb.IsSet)
            {
                return;
            }

            var sb = honeyComb?.FindResource("OnMouseLeave") as Storyboard;
            sb?.Begin();

            var horHint = FindPathByName("horHintArea", honeyComb?.canvas1.Children);
            if (horHint != null && horHint.Opacity > 0)
            {
                horHint.IsHitTestVisible = false;
                horHint.BeginStoryboard(this.fadeOut);
            }

            var leftHint = FindPathByName("leftHintArea", honeyComb?.canvas1.Children);
            if (leftHint != null && leftHint.Opacity > 0)
            {
                leftHint.IsHitTestVisible = false;
                leftHint.BeginStoryboard(this.fadeOut);
            }

            var rightHint = FindPathByName("rightHintArea", honeyComb?.canvas1.Children);
            if (rightHint != null && rightHint.Opacity > 0)
            {
                rightHint.IsHitTestVisible = false;
                rightHint.BeginStoryboard(this.fadeOut);
            }

            if (this.hint != null && this.hint.Opacity > 0)
            {
                this.hint.BeginStoryboard(this.fadeOut);
            }
        }

        private void H1MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.hint.Visibility = Visibility.Hidden;
            this.OnPropertyChanged("Hint");

            var honeyComb = ((Canvas)sender).Parent as HoneyComb;
            if (honeyComb != null && honeyComb.IsSet)
            {
                return;
            }

            if (honeyComb.Score[0] + honeyComb.Score[1] != 0 && honeyComb.Score[1] + honeyComb.Score[2] != 0
                && honeyComb.Score[0] + honeyComb.Score[2] != 0)
            {
                return;
            }

            var sb = honeyComb.FindResource("OnMouseLeftDown") as Storyboard;
            sb.Begin();

            honeyComb.IsSet = true;
            var num = honeyComb.num;

            this.Score = honeyComb.Score.Sum();
            this.OnPropertyChanged("Score");

            if (this.isMyTurn)
            {
                this.youTxt.FontWeight = FontWeights.Regular;
                this.compTxt.FontWeight = FontWeights.UltraBold;

                this.YouScore += honeyComb.Score.Sum();
                this.DrawLines(num, honeyComb.Score);
                this.isMyTurn = false;
            }
            else
            {
                this.youTxt.FontWeight = FontWeights.UltraBold;
                this.compTxt.FontWeight = FontWeights.Regular;

                this.CompScore += honeyComb.Score.Sum();
                this.DrawLines(num, honeyComb.Score);
                this.isMyTurn = true;
            }

            this.state |= (ulong)1 << num;
            this.OnPropertyChanged("State");
        }

        private void HorLeftButtonUp(object sender, MouseButtonEventArgs e)
        {


            this.hint.Visibility = Visibility.Hidden;
            this.OnPropertyChanged("Hint");

            var horPath = (Path)sender;
            var honeyComb = ((Canvas)horPath.Parent).Parent as HoneyComb;


            if (honeyComb != null && this.gameLogic1.scores[0, this.gameLogic1.horRows[honeyComb.num]])
            {
                return;
            }

 

            var sb = honeyComb?.FindResource("OnMouseLeftDown") as Storyboard;
            sb?.Begin();

            honeyComb.IsSet = true;
            var num = honeyComb.num;

            this.Score = honeyComb.Score[0];
            this.OnPropertyChanged("Score");

            if (this.isMyTurn)
            {
                this.youTxt.FontWeight = FontWeights.Regular;
                this.compTxt.FontWeight = FontWeights.UltraBold;

                this.YouScore += honeyComb.Score[0];
                this.DrawHorLine(honeyComb.Score, num);
                this.isMyTurn = false;
            }
            else
            {
                this.youTxt.FontWeight = FontWeights.UltraBold;
                this.compTxt.FontWeight = FontWeights.Regular;

                this.CompScore += honeyComb.Score[0];
                this.DrawHorLine(honeyComb.Score, num);
                this.isMyTurn = true;
            }
            this.gameLogic1.scores[0, this.gameLogic1.horRows[honeyComb.num]] = true;
            horPath.BeginStoryboard(this.fadeOut);
            FindPathByName("leftHintArea", honeyComb.canvas1.Children).BeginStoryboard(this.fadeOut);
            FindPathByName("rightHintArea", honeyComb.canvas1.Children).BeginStoryboard(this.fadeOut);
            this.state |= (ulong)1 << num;
            this.OnPropertyChanged("State");
        }

        private void InitGame()
        {
            if (this.isMyTurn)
            {
                this.youTxt.FontWeight = FontWeights.UltraBold;
                this.compTxt.FontWeight = FontWeights.Regular;
            }
            else
            {
                this.youTxt.FontWeight = FontWeights.Regular;
                this.compTxt.FontWeight = FontWeights.UltraBold;
            }

            this.hintSb = new Storyboard();
            var da = new DoubleAnimation(0, 1, new Duration(new TimeSpan(0, 0, 0, 0, 1100)));
            Storyboard.SetTargetProperty(da, new PropertyPath("(UIElement.Opacity)"));
            this.hintSb.Children.Add(da);

            this.fadeIn = new Storyboard();
            var da1 = new DoubleAnimation(0, 0.8, new Duration(new TimeSpan(0, 0, 0, 0, 1100)));
            Storyboard.SetTargetProperty(da1, new PropertyPath("(UIElement.Opacity)"));
            da1.EasingFunction = new ExponentialEase { Exponent = 9 };
            this.fadeIn.Children.Add(da1);

            this.fadeOut = new Storyboard();
            var da2 = new DoubleAnimation(0.8, 0.0, new Duration(new TimeSpan(0, 0, 0, 0, 1100)));
            Storyboard.SetTargetProperty(da2, new PropertyPath("(UIElement.Opacity)"));
            da2.EasingFunction = new ExponentialEase { Exponent = 9 };
            this.fadeOut.Children.Add(da2);
        }

        private void LeftLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.hint.Visibility = Visibility.Hidden;
            this.OnPropertyChanged("Hint");

            var leftPath = (Path)sender;
            var honeyComb = ((Canvas)leftPath.Parent).Parent as HoneyComb;

            if (honeyComb.IsSet)
            {
                return;
            }

            var sb = honeyComb.FindResource("OnMouseLeftDown") as Storyboard;
            sb.Begin();

            honeyComb.IsSet = true;
            var num = honeyComb.num;

            this.Score = honeyComb.Score[1];
            this.OnPropertyChanged("Score");

            if (this.isMyTurn)
            {
                this.youTxt.FontWeight = FontWeights.Regular;
                this.compTxt.FontWeight = FontWeights.UltraBold;

                this.YouScore += honeyComb.Score[1];
                this.DrawLeftLine(honeyComb.Score, num);
                this.isMyTurn = false;
            }
            else
            {
                this.youTxt.FontWeight = FontWeights.UltraBold;
                this.compTxt.FontWeight = FontWeights.Regular;

                this.CompScore += honeyComb.Score[1];
                this.DrawLeftLine(honeyComb.Score, num);
                this.isMyTurn = true;
            }

            leftPath.BeginStoryboard(this.fadeOut);
            FindPathByName("horHintArea", honeyComb.canvas1.Children).BeginStoryboard(this.fadeOut);
            FindPathByName("rightHintArea", honeyComb.canvas1.Children).BeginStoryboard(this.fadeOut);

            this.state |= (ulong)1 << num;
            this.OnPropertyChanged("State");
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.DrawHoneyPot();
        }

        private void Ph_MouseEnter(object sender, MouseEventArgs e)
        { var honeyComb = (HoneyComb)((Canvas)((Path)sender).Parent).Parent;

            if (this.gameLogic1.scores[0, this.gameLogic1.horRows[honeyComb.num]])
            {
                return;
            }

            this.Hint = honeyComb.Score[0];
            this.OnPropertyChanged("Hint");

            this.HintLeft = Canvas.GetLeft(honeyComb) + (honeyComb.honeycomb.ActualWidth / 2) - 7
                            - (this.Hint > 9 ? 5 : 0);
            this.HintTop = Canvas.GetTop(honeyComb) + (honeyComb.honeycomb.ActualHeight / 2) - 10;
            this.OnPropertyChanged("HintTop");
            this.OnPropertyChanged("HintLeft");
            this.hint.BeginStoryboard(this.hintSb);
            
        }

        private void Ph_MouseLeave(object sender, MouseEventArgs e)
        {
            if (this.hint != null && this.hint.Opacity > 0)
            {
                this.hint.BeginStoryboard(this.fadeOut);
            }
        }

        private void Pl_MouseEnter(object sender, MouseEventArgs e)
        {
            var honeyComb = (HoneyComb)((Canvas)((Path)sender).Parent).Parent;

            if (this.gameLogic1.scores[1, this.gameLogic1.leftRows[honeyComb.num]])
            {
                return;
            }


            this.Hint = honeyComb.Score[1];
            this.OnPropertyChanged("Hint");



            this.HintLeft = Canvas.GetLeft(honeyComb) + (honeyComb.honeycomb.ActualWidth / 2) - 7
                            - (this.Hint > 9 ? 5 : 0);
            this.HintTop = Canvas.GetTop(honeyComb) + (honeyComb.honeycomb.ActualHeight / 2) - 10;
            this.OnPropertyChanged("HintTop");
            this.OnPropertyChanged("HintLeft");
            this.hint.BeginStoryboard(this.hintSb);

        }

        private void Pr_MouseEnter(object sender, MouseEventArgs e)
        {
            var honeyComb = (HoneyComb)((Canvas)((Path)sender).Parent).Parent;
            this.Hint = honeyComb.Score[2];
            this.OnPropertyChanged("Hint");

            this.HintLeft = Canvas.GetLeft(honeyComb) + (honeyComb.honeycomb.ActualWidth / 2) - 7
                            - (this.Hint > 9 ? 5 : 0);
            this.HintTop = Canvas.GetTop(honeyComb) + (honeyComb.honeycomb.ActualHeight / 2) - 10;
            this.OnPropertyChanged("HintTop");
            this.OnPropertyChanged("HintLeft");
            this.hint.BeginStoryboard(this.hintSb);
        }

        private void rightLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.hint.Visibility = Visibility.Hidden;
            this.OnPropertyChanged("Hint");

            var rightPath = (Path)sender;
            var honeyComb = ((Canvas)rightPath.Parent).Parent as HoneyComb;
            if (honeyComb.IsSet) return;
            var sb = honeyComb.FindResource("OnMouseLeftDown") as Storyboard;
            sb.Begin();

            honeyComb.IsSet = true;
            var num = honeyComb.num;

            this.Score = honeyComb.Score[2];
            this.OnPropertyChanged("Score");

            if (this.isMyTurn)
            {
                this.youTxt.FontWeight = FontWeights.Regular;
                this.compTxt.FontWeight = FontWeights.UltraBold;

                this.YouScore += honeyComb.Score[2];
                this.DrawRightLine(honeyComb.Score, num);
                this.isMyTurn = false;
            }
            else
            {
                this.youTxt.FontWeight = FontWeights.UltraBold;
                this.compTxt.FontWeight = FontWeights.Regular;

                this.CompScore += honeyComb.Score[2];
                this.DrawRightLine(honeyComb.Score, num);
                this.isMyTurn = true;
            }

            rightPath.BeginStoryboard(this.fadeOut);

            this.state |= (ulong)1 << num;
            this.OnPropertyChanged("State");
        }
    }
}